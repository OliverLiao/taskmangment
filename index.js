const { ApolloServer } = require('apollo-server');
const mongoose = require('mongoose');
const typeDefs = require('./schema');
const taskSchema = require('./model/task');
const employeeSchema = require('./model/employee');
const resolvers = require('./resolver');
const { getToken, verify } = require('./lib/Authorization');

require('dotenv').config();

async function startGraphQLServer(){

    await mongoose.connect(
        process.env.mongoURL, 
        { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false })
        .then(res => console.log("mongoDB connect success"))
    
    const server = new ApolloServer({ 
        typeDefs, 
        resolvers,
        context: async ({req})=>{
            let token = getToken(req);
            let currentUser = verify(token);
            // console.log(currentUser)
            return { currentUser, taskSchema, employeeSchema }
        }
    });

    server.listen().then(({ url }) => {
        console.log(`🚀  Server ready at ${url}`);
    });
}

startGraphQLServer();



