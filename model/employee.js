const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const employeeSchema = new Schema({
    name: String,
    email: String,
    role: String,
    tel: String,
    dept: String,
    position: Number,
    password: String
})

module.exports = mongoose.model('employee', employeeSchema)