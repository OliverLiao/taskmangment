const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const taskSchema = new Schema({
    name: String,
    desc: String,
    priority: String,
    deadline: Number,
    publishDate: Number,
    publisher: String,
    executor: String,
    finishedDate: Number,
    isAssign: Boolean,
    isFinished: Boolean
})

module.exports = mongoose.model('task', taskSchema);