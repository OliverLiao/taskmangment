const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const getToken = req => {
    let token = req.headers.authorization;
    return token;
}

const verify = token => {
    if(!token) return false
    return jwt.verify(token, process.env.privateKey, function(error, decode){
        if (error) throw new Error("Token is invaild")
        return decode
    })
}

const signToken = (password, hash, User) => {
    return bcrypt.compare(password, hash).then(function (res) {
        let { name, email, role, password } = User
        if (res) {
            return jwt.sign({ name, email, role, password }, process.env.privateKey);
        } else {
            throw new Error(JSON.stringify(`something error`))
        }
    }).catch(error => {
        throw new Error(JSON.stringify(error))
    });
}


const hash = (password) => bcrypt.hash(password, saltRounds).then(function (hash) {
        return password = hash
    })

module.exports = {
    getToken,
    verify,
    signToken,
    hash
}