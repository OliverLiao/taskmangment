const { gql } = require('apollo-server');

const typeDefs = gql`
    enum ROLE{
        superAdmin
        manager
        developer
    }

    input CreateNewTask{
        taskName: String!,
        desc: String!,
        deadline: String!
        priority: String!,
        publisher: String!,
        executor: String
    }

    input FilterTask{
        isAssign:Boolean 
        isFinished:Boolean
    }

    input AddNewEmployee{
        name: String!
        email: String!
        role: ROLE
        tel: String!
        dept: String
        position: Int
        password: String!
    }

    type Query{
        allTasks(filter:FilterTask): [task!]!
        allemployeers: [Employee!]!
        currentUser:AuthPayload
    }

    type Mutation{
        login(email: String! password:String!): AuthPayload!
        createEmployee(input:AddNewEmployee): Employee!
        createTask(input:CreateNewTask!): task!
        closeTask(ID: String! status:Boolean!): task!
        assign(ID: String! executor:String!): task!
    }

    type AuthPayload{
        token: String
        user: User
    }

    type User{
        name: String,
        email: String,
        role: String
    }

    type Employee{
        _id:String,
        name: String!,
        email: String!,
        role: String!,
        tel: String!,
        dept: String,
        position: Int,
        assigned:[task],
        finished:[task]
    }

    type task{
        _id: String!,
        name: String!,
        desc: String!,
        priority: String!,
        deadline: Float!,
        publishDate: Float!,
        publisher: String!,
        executor: String,
        finishedDate: Float,
        isAssign: Boolean!,
        isFinished: Boolean!
    }
`

module.exports = typeDefs

