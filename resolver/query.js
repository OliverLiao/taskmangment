const Query = {
    allTasks: (parent, args, { currentUser, taskSchema }) => {
        // if (!currentUser) throw new Error('no permission')
        if (args.filter !== undefined) {
            return taskSchema.find({ $or:[
                {
                    isAssign: args.filter.isAssign 
                },
                {
                    isFinished: args.filter.isFinished 
                }
            ]}).then(res => res)
        }
        return taskSchema.find().then(res => res)
    },
    allemployeers: (parent, args, { currentUser, employeeSchema }) => {
        // if (!currentUser) throw new Error('no permission')
        return employeeSchema.find().then(res => res)
    }
}

module.exports = Query;