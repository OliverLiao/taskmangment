const Query = require('./query');
const Mutation = require('./mutation');
const taskSchema = require('../model/task');

const resolvers = {
    Query,
    Mutation,
    Employee: {
        assigned: async (parent, args) => {
            let allTasks = await taskSchema.find({}).then(res=> res)
            return allTasks.filter(task => {
                if (task.executor == parent._id) {
                    return task
                }
            })
        },
        finished: async (parent, args) => null
    }
}

module.exports = resolvers;