const { ObjectID } = require('mongodb');
const bcrypt = require('bcrypt');
const { signToken, hash } = require('../lib/Authorization');

const Mutation = {
    login: async (parent, args, { employeeSchema }) => {
        let User = await employeeSchema.findOne({ email: args.email }).then(employee=> employee)
        let token = await signToken(args.password, User.password, User)
        return {
            token: token,
            user: User
        }
    },
    createEmployee: async (parent, args, { employeeSchema }) => {
        let password = await hash(args.input.password);
        let newEmployee = {
            name: args.input.name,
            email: args.input.email,
            role: args.input.role,
            tel: args.input.tel,
            dept: args.input.tel,
            position: args.input.position,
            password: password
        }
        return new employeeSchema(newEmployee).save().then( res => res )
    },
    createTask: (parent, args, { taskSchema }) => {
        console.log(args)
        let newTask = {
            name: args.input.taskName,
            desc: args.input.desc,
            priority: args.input.priority,
            deadline: new Date().getTime(args.input.deadline),
            publishDate: new Date().getTime(),
            publisher: args.input.publisher,
            executor: args.input.executor,
            finishedDate: new Date().getTime(),
            isAssign: false,
            isFinished: false
        }
        return new taskSchema(newTask).save().then(res => res );
    },
    closeTask: async (parent, args, { taskSchema }) => { 
        return await taskSchema.findOneAndUpdate(
                { _id: ObjectID(args.ID)},
                { $set: { isFinished: args.status }},
                { upsert: true})
            .then(res => res );
    },
    assign: async (parent, args, { taskSchema }) => {
        return await taskSchema.findOneAndUpdate(
            { _id: ObjectID(args.ID) },
            { $set: { 
                isAssign: true,
                executor: args.executor
            }},
            { upsert: true })
            .then(res => res);
    }
}

module.exports = Mutation;